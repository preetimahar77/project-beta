from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=13)


class Record(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="records",
        on_delete=models.CASCADE,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="records",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="records",
        on_delete=models.CASCADE,
    )
    price = models.CharField(max_length=15)


class SaleList(models.Model):
    record = models.ForeignKey(
        Record,
        related_name="sale_lists",
        on_delete=models.CASCADE,
    )

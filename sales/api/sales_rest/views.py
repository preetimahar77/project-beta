from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, Record, SaleList


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin"
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]


class RecordEncoder(ModelEncoder):
    model = Record
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }


class SaleListEncoder(ModelEncoder):
    model = SaleList
    properties = [
        "record",
        "id",
    ]
    encoders = {
        "record": RecordEncoder(),

    }


@require_http_methods(["GET", "POST"])
def api_list_sales_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Invalid sales person"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sales person id"},
                status=400
            )
    elif request.method == "DELETE":
            count, _ = SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=pk).update(**content)
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Invalid customer"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sales person id"},
                status=400
            )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_records(request):
    if request.method == "GET":
        records = Record.objects.all()
        return JsonResponse(
            {"records": records},
            encoder=RecordEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)

            automobile = AutomobileVO.objects.get(vin = content["automobile"])
            content["automobile"]= automobile
            sales_person = SalesPerson.objects.get(id = content["sales_person"])
            content["sales_person"] = sales_person
            customer = Customer.objects.get(id = content["customer"])
            content["customer"] = customer

            record = Record.objects.create(**content)
            return JsonResponse(
                record,
                encoder=RecordEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Invalid sales record"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_record(request, pk):
    if request.method == "GET":
        try:
            record = Record.objects.get(id=pk)
            return JsonResponse(
                record,
                encoder=RecordEncoder,
                safe=False,
            )
        except record.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Record"},
                status=400,
            )
    elif request.method == "DELETE":
            count, _ = Record.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Record.objects.filter(id=pk).update(**content)
        record = Record.objects.get(id=pk)
        return JsonResponse(
            record,
            encoder=RecordEncoder,
            safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sale_lists = SaleList.objects.all()
        return JsonResponse(
            {"sale_lists": sale_lists},
            encoder=SaleListEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            record = Record.objects.get(id = content["record"])
            content["record"] = record

            sale_list = SaleList.objects.create(**content)
            return JsonResponse(
                sale_list,
                encoder=SaleListEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Invalid sales record"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET"])
def show_sales_history(request, pk):
    if request.method == "GET":
        records = Record.objects.filter(sales_person_id=pk)
        return JsonResponse(
            {"records": records},
            encoder=RecordEncoder,
            safe=False
        )
    else:
        return JsonResponse(
            {"message": "Invalid Method"},
            status=400,
        )

# CarCar

Here we have the Front-end for the inventory:-


Show a list of manufacturers - ListManufacturers.js
Create a manufacturer - ManufacturerForm.js
Show a list of vehicle models - VehicleModelsList.js
Create a vehicle model - VehicleModels.js
Show a list of automobiles in inventory - AutomobileList.js
Create an automobile in inventory - AutomobileForm.js
# Team

### Sales
The Sales page allows you to access the following:
- Create a Sales Person
- Create a Customer
- Create a Sales Record
- View a list of all sales
- Access sale history by a given sales person's employee number

## Initial Data (Sales)
In order to create a sale, the app needs inventory.
Inventory:
- Create at least 2 Manufacturers
- Create at least 2 Vehicle Models
- Finally, create an Automobile.

Sales:
- Create at least 2 Sales People
- Create at least 2 Customers
- Finally, create a sales Record

* Jonas Tengesdal - Sales
* Preeti Mahar - Services

# Design

![Design](/docs/Design.png)

# Setup
steps: After being given the 'maintainer' status by my partner who forked project-beta from the lab.I did :-

```
git clone [repository url]
cd project-beta
docker-compose build
docker-compose up
```
## Usage
The React App would be accessed via http://localhost:3000 .

The home page contains links to access inventory information. The navbar contains links to Home Page, Services Page and Sales Page.
### Home Page
Home Page contains links to access Inventory related pages (see below).

#### Manufacturer List
- Shows the List of all Manufacturers
- Link to add a new manufacturer
#### Vehicle Model List
- Shows the List of all Vehicle Models
- Link to add a new Vehicle Model
#### Automobile Inventory List
- Shows the List of all Automobiles in the Inventory
- Link to add a new Automobile to the Inventory

### Services
Services page shows the list of all active service appointments and also have links to access following
- Create an Appointment
- Create a new Technician
- Search Vehicles Service History

## Filing Initial Data

Start from Home Page, and create two Manufacturers. Now, create two Vehicle Models and then create an Automobile for the inventory.

For Services, start by creating two technicians, and then create a new appointment.

# Microservices
## Service Microservice

Explain your models and integration with the inventory
microservice, here.

1 - Here we have the service appointment models in the :- service/api/service_rest/models.py.
Here we have the Appointment, Technician and the AutomobileV0 Models. We have a set of variables defined inside each model and a field parameter is assigned to each that defines their type like integer, date, char, etc.

2 - After making the models , we need to: docker execute -it service-api-1 bash // to connect to the running service that contains our API service.
3 - python manage.py makemigrations // Typed at the container command prompt. Made sure that it generates the required migrations.
4 -  python manage.py migrate // to apply the migration.
5 - Stop and start poller service using Docker Desktop.

Services microservices can be accessed using port 8080.

#### Technicians

Technicians model has following views.

|  Action | Method  | URL  |
|---|---|---|
| List all Technicians  | GET  | http://localhost:8080/service/api/list  |
| Create new Technician  | POST  | http://localhost:8080/service/api/create  |

To create a Technician, following json can be used as payload to create enpoint using POST method.
```
{
    "name": "Gary Smith",
    "employee_no": "8766"
}

```

List of technicians will be retrieved via GET method in following format.
```
{
    "technicians": [
        {
            "name": "paul",
            "employee_no": 33,
            "id": 1
        },
        {
            "name": "james",
            "employee_no": 44,
            "id": 2
        }
    ]
}
```

#### Appointmennts

Appointments model has following views.

|  Action | Method  | URL  |
|---|---|---|
| List all active Appoinments  | GET  | http://localhost:8080/service/api/  |
| Create new Appointment  | POST  | http://localhost:8080/service/api/appointment/create  |
| Update Appointment as cancelled or Finished | PUT | http://localhost:8080/service/api/appointment/:id/update |

To create an Appoinment, following json can be used as payload to create enpoint using POST method.
```
{
    "vin": "123456",
    "customer_name": "Jeremy",
    "appointment_time": "2023-03-03T08:39",
    "technician": "6"
    "reason": "Coolant Low"
}
```

List of all active Appoinments will be retrieved via GET method in following format. This list will not return any appoinment which has been cancelled or marked finished.
```
{
    "technicians": [
        {
            "name": "paul",
            "employee_no": 33,
            "id": 1
        },
        {
            "name": "james",
            "employee_no": 44,
            "id": 2
        }
    ]
}
```

In order to update the status for an Appoinment to 'Cancel', following payload should be sent using PUT method.
```
{
    "status":"CL"
}
```

In order to update the status for an Appoinment to 'Finished', following payload should be sent using PUT method.
```
{
    "status":"FN"
}
```
#### Service History

Service History can be obtained for a specific automobile by using its VIN number.

|  Action | Method  | URL  |
|---|---|---|
| List all Service Appoinments  | GET  | http://localhost:8080/service/api/vin/:vin/list/  |

Above request will return following json with a list of all appointments for the specified vin.
```
{
	"appointments": [
		{
			"appointment_time": "2023-02-15T20:40:00+00:00",
			"reason": "test run",
			"status": "PD",
			"customer_name": "James May",
			"vin": "ggr5444567890",
			"vip_customer": false,
			"technician": {
				"name": "veggies",
				"employee_no": 67,
				"id": 5
			},
			"id": 4
		}
	]
}
```

The views are made in the:- service/api/service_rest/views.py.We have the AutomobileV0Encoder, TechnicianEncoder,etc and the views.
The create_technician , list_technician  views in the back-end help in creating a new technician and listing names and emp.no. of technicians respectively.
The create_service_appointment, list_service_appointment and show_service_history views help in creating a new appointment, listing service appointments and showing the service history respectively.
The update_appointment is to update the list according to the status of it.
Their urls are made in the:- service/api/service_rest/api_urls.py .The urls indicate the path that is invoked when the respective view function is invoked.
The forms associated with them which go into the front-end are in the :-ghi/app/src , namely:
AppointmentForm.js, ListAppointmentForm.js, TechnicianForm.js, etc. It shows how we use the useEffect, useState methods in react in a form function that we define in js to fetch the url and create response. Then render the html in the return to make the form in the front-end.

- After making changes to the views and url files, needed to build the containers again and rerun everything by:-
- docker container prune -f // to delete all containers
- docker-compose build  // to build all images
- docker-compose up    // to run the containers

- Did - git add .
- git commit - m "text"
- git push
- git checkout service-branch
- git pull
- git push
- git merge main
- git checkout main
- git pull
- git merge service-branch
- git push
from time to time to add changes to the gitlab. Later resolved merge conflicts, saved the files and pushed changes to the origin/main.
## Sales microservice

Explain your models and integration with the inventory
microservice, here.

- The Sales Record models are located in sales/api/sales_rest/models.py.
- Once models are created, either run the command "docker execute -it service-api-1 bash" in your code compiler's terminal, or
open the terminal in the respective running container in Docker Desktop to connect to your API service.
- Run the command "python manage.py makemigrations" in your preferred terminal, watching for your migrations to generate.
- Onc eyou know that the migrations have generated, run the command "python manage.py migrate" in order to complete your migrations.
- In the Docker Desktop, stop/start the container that just had migrations applied to.

The Sales microservice development container run on port 8090.

NOTE: The Sales Person History in http://localhost:8090/api/sale_list/<int:id>/ (API) and http://localhost:3000/sale_lists/history/ (URL) are incomplete. The back-end is functional, however, the front-end doesn't access the sales records associated with the respective sales person.

### Note: The following URL endpoints were accessed through Insomnia

#### Sales People

The SalesPerson model has following views.

|  Action | Method  | URL  |
|---|---|---|
| List sales people  | GET  | http://localhost:8090/api/sales_people/ |
| Create sales person  | POST  | http://localhost:8090/api/sales_people/  |
| Get sales person details | GET  | http://localhost:8090/api/sales_people/<int:pk>/  |
| Delete sales person | DELETE  | http://localhost:8090/api/sales_people/<int:pk>/  |

To create a Sales Person, follow this template using JSON with POST method.

{
	"name": "Alec",
	"employee_number": 2
}

The list of Sales People will be accessed via GET method.

{
	"sales_people": [
		{
			"name": "Erin",
			"employee_number": 1,
			"id": 1
		},
		{
			"name": "Alec",
			"employee_number": 2,
			"id": 2
		},
		{
			"name": "John",
			"employee_number": 3,
			"id": 3
		}
	]
}

#### Customers

The Customer model has following views.

|  Action | Method  | URL  |
|---|---|---|
| List customers  | GET  | http://localhost:8090/api/customers/ |
| Create a customer | POST  | http://localhost:8090/api/customers/  |
| Get customer details | GET  | http://localhost:8090/api/customers/<int:pk>/  |
| Delete customer | DELETE  | http://localhost:8090/api/customers/<int:pk>/  |

To create a Customer, follow this template using JSON with POST method.

{
	"name": "Erin",
	"address": "111 Test ave",
	"phone_number": "111-222-333"
}

The list of Customers will be accessed via GET method.

{
	"customers": [
		{
			"name": "Dan",
			"address": "123 1st st se",
			"phone_number": "123-123-123",
			"id": 1
		},
		{
			"name": "Erin",
			"address": "111 Test ave",
			"phone_number": "111-222-333",
			"id": 2
		},
    ]
}

#### Sales Records

The Customer model has following views.

|  Action | Method  | URL  |
|---|---|---|
| List records  | GET  | http://localhost:8090/api/records/ |
| Create a record | POST  | http://localhost:8090/api/records/  |
| Get record details | GET  | http://localhost:8090/api/records/<int:pk>/  |
| Delete record | DELETE  | http://localhost:8090/api/records/<int:pk>/  |

To create a Record, follow this template using JSON with POST method.

{
	"automobile": "111222333",
	"sales_person": 3,
	"customer": 2,
	"price": 3434
}

The list of Records will be accessed via GET method.

{
	"automobile": {
		"import_href": "",
		"vin": "111222333"
	},
	"sales_person": {
		"name": "John",
		"employee_number": 3,
		"id": 3
	},
	"customer": {
		"name": "Erin",
		"address": "111 Test ave",
		"phone_number": "111-222-333",
		"id": 2
	},
	"price": 3434,
	"id": 22
}
....and so on

#### List of Sales

The SaleList model has following views.

|  Action | Method  | URL  |
|---|---|---|
| List sales  | GET  | http://localhost:8090/api/sale_lists/ |
| Create a sale | POST  | http://localhost:8090/api/sale_lists/  |

To create a Sale, follow this template using JSON with POST method.
The Record ID is all that's required!!!

{
	"record": 1
}

The list of Sales will be accessed via GET method.

{
	"sale_lists": [
		{
			"record": {
				"automobile": {
					"import_href": "",
					"vin": "111222333"
				},
				"sales_person": {
					"name": "Erin",
					"employee_number": 1,
					"id": 1
				},
				"customer": {
					"name": "Dan",
					"address": "123 1st st se",
					"phone_number": "123-123-123",
					"id": 1
				},
				"price": "1234.00",
				"id": 1
			},
			"id": 1
		},
        ....and so on
    ]
}

#### Sales Person History

Sales Person History can be accessed via sales person id/employee_number.

|  Action | Method  | URL  |
|---|---|---|
| Get sales person history details | GET  | http://localhost:8090/api/sale_list/<int:id>/  |

You will receive a list of all sales records attached to the inputted id/employee_number
If http://localhost:8090/api/sale_list/3/ is used, the out put will be:

{
	"records": [
		{
			"automobile": {
				"import_href": "",
				"vin": "111222333"
			},
			"sales_person": {
				"name": "John",
				"employee_number": 3,
				"id": 3
			},
			"customer": {
				"name": "sd",
				"address": "ds",
				"phone_number": "3455",
				"id": 3
			},
			"price": "1234",
			"id": 20
		},
		{
			"automobile": {
				"import_href": "",
				"vin": "111222333"
			},
			"sales_person": {
				"name": "John",
				"employee_number": 3,
				"id": 3
			},
			"customer": {
				"name": "Dan",
				"address": "123 1st st se",
				"phone_number": "123-123-123",
				"id": 1
			},
			"price": "1234",
			"id": 21
		},
		{
			"automobile": {
				"import_href": "",
				"vin": "111222333"
			},
			"sales_person": {
				"name": "John",
				"employee_number": 3,
				"id": 3
			},
			"customer": {
				"name": "Erin",
				"address": "111 Test ave",
				"phone_number": "111-222-333",
				"id": 2
			},
			"price": "3434",
			"id": 22
		}
	]
}

The views are made in the sales/api/sales_rest/views.py
The urls are made in the sales/api/sales_rest/api_urls.py
The associated forms are in the ghi/app/src:

SalesPersonForm.js
CustomerForm.js
RecordForm.js

After making changes to the views and url files, it's recommended that you stop and rebuild your containers.

- docker container prune -f (if inexplicable errors are present) Note: All RESTFUL API data will be removed.
- docker-compose down
- docker-compose build
- docker-compose up

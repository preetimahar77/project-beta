import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()


# Import models from service_rest, here.
from service_rest.models import AutomobileV0


def get_inventory():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for vehicle in content["autos"]:
        AutomobileV0.objects.update_or_create(
            import_href=vehicle["href"],
            defaults={
                'vin': vehicle["vin"],
                'year': vehicle["year"],
                'color': vehicle["color"]
            },
        )


def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_inventory()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()

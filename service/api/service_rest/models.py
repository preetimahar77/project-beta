from django.db import models
from django.urls import reverse


class Technician(models.Model):
    name = models.CharField(max_length=150)
    employee_no = models.PositiveIntegerField()


class Appointment(models.Model):
    appointment_time = models.DateTimeField()
    reason = models.CharField(max_length=150)
    PENDING = 'PD'
    CANCELLED = 'CL'
    FINISHED = 'FN'
    APPT_STATUS_CHOICES = [
        (PENDING, 'Pending'),
        (CANCELLED, 'Cancelled'),
        (FINISHED, 'Finished')
    ]
    status = models.CharField(
        max_length=10,
        choices=APPT_STATUS_CHOICES,
        default=PENDING)
    customer_name = models.CharField(max_length=150)
    vin = models.CharField(max_length=50)
    vip_customer = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )


class AutomobileV0(models.Model):
    import_href = models.URLField()
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

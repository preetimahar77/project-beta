from django.urls import path
from service_rest.views import (
    show_service_history,
    list_service_appointment,
    create_technician,
    create_service_appointment,
    update_appointment,
    list_technician)

urlpatterns = [
    path(
        '',
        list_service_appointment,
        name='list_service_appointment'),
    path('vin/<str:vin>/list', show_service_history, name='show_service_history'),
    path('technician/create', create_technician, name='create_technician'),
    path('technician/list', list_technician, name='list_technician'),
    path(
        'appointment/create',
        create_service_appointment,
        name='create_service_appointment'),
    path(
        'appointment/<int:id>/update',
        update_appointment,
        name='update_appointment'),
]

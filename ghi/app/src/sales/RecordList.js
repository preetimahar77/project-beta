import React from "react";


function RecordList() {
    return (
        <div>
            <h2 className="mt-5"><b>Sales Records</b></h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Automobile</th>
                        <th>Sales Person</th>
                        <th>Customer</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    )
}

export default RecordList;

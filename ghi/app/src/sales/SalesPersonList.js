import React from "react";

function SalesPersonList() {
    return (
        <div>
            <h2 className="mt-5"><b>Sales People</b></h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee Number</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    )
}

export default SalesPersonList;

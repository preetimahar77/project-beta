import React from "react";


function CustomerList() {
    return (
        <div>
            <h2 className="mt-5"><b>Customers</b></h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    )
}

export default CustomerList;

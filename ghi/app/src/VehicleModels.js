import { React, useEffect, useState } from "react";

function VehicleModelsForm() {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [manufacturer_id, setManufacturer_id] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer_id(value);
    }
    const [picture_url, setPicture_url] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture_url(value);
    }

    const [manufacturers, setManufacturers] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.picture_url = picture_url;
        data.manufacturer_id = manufacturer_id;

        const appointmentUrl = `http://localhost:8100/api/models/`;
        const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                "Content-Type": "application/json",
                },
            };
        const response = await fetch(appointmentUrl, fetchConfig, {mode: 'no-cors'});
        if (response.ok) {
            setName('');
            setManufacturer_id('');
            setPicture_url('');
        }
    }
    // fetchData, useEffect, and return methods...
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }
    useEffect(() => {
        fetchData();
        }, [] );

        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new Vehicle Model</h1>
                <form onSubmit={handleSubmit} id="create-vehicle-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="name" required type="text" name="name" value={name} id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handleManufacturerChange} required name="manufacturer" id="manufacturer" value={manufacturer_id} className="form-select">
                            <option value="">Choose a manufacturer</option>
                            {manufacturers.map(manufacturer => {
                                return (
                                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                            );
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureChange} placeholder="url" required type="text" name="picture_url" value={picture_url} id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <button className="btn btn-primary">Create a Vehicle</button>
                </form>
                </div>
                </div>
            </div>
                );
                }

export default VehicleModelsForm;

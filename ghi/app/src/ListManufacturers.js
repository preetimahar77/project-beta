import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
function ManufacturerList() {
    const [ManufacturerList, setManufacturerList] = useState([]);
    useEffect(() => {
        loadManufacturerList();
    }, []);

    async function loadManufacturerList() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if(response.ok) {
            const data = await response.json();
            setManufacturerList(data.manufacturers);
        }
        else {
            console.error(response);
        }
    }
return (
    <div>
        <br />
        <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
            <Link to='/inventory/manufacturers/new' className="btn btn-primary btn-lg px-4 gp-3">Create a Manufacturer</Link>
        </div>
        <br />
        <h2>Manufacturers</h2>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Manufacturer Name</th>
        </tr>
        </thead>
        <tbody>
            {ManufacturerList.map(manufacturer =>
                <tr key={ manufacturer.name }>
                    <td>{ manufacturer.name }</td>
                </tr>
            ) }
        </tbody>
    </table>
    </div>
    );
}
export default ManufacturerList;

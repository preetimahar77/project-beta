import { React, useState } from "react";

function TechnicianForm() {

    const [name, setName] = useState('');
    const handlenameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [employee_no, setEmployee_no] = useState('');
    const handleemployee_noChange = (event) => {
        const value = event.target.value;
        setEmployee_no(value);
    }


const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.name = name;
    data.employee_no = employee_no;

    const technicianUrl = `http://localhost:8080/service/api/technician/create`;
        const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                "Content-Type": "application/json",
                },
            };
        const response = await fetch(technicianUrl, fetchConfig, {mode: 'no-cors'});
        if (response.ok) {
            setName('');
            setEmployee_no('');
        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new Technician</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                <input onChange={handlenameChange} placeholder="name" required type="text" name="name" value={name} id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleemployee_noChange} placeholder="employee_no" required type="text" name="employee_no" value={employee_no} id="employee_no" className="form-control"/>
                <label htmlFor="employee_no">Employee Number</label>
                </div>
                <button className="btn btn-primary">Create a Technician</button>
                </form>
                </div>
                </div>
                </div>
    );
}
export default TechnicianForm;

import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/service/appointments/">Services</NavLink>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><NavLink className="dropdown-item" aria-current="page" to="/sales_people/new/">Create a Sales Person</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="/customers/new/">Create a Customer</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="/records/new/">Create a Sale Record</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="/sale_lists/">List of Sales</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="/sale_lists/history/">Sales Person History</NavLink></li>
                </ul>
              </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;

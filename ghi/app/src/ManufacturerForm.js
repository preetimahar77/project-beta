import { React, useEffect, useState } from "react";

function ManufacturerForm() {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;

        const appointmentUrl = `http://localhost:8100/api/manufacturers/`;
        const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                "Content-Type": "application/json",
                },
            };
        const response = await fetch(appointmentUrl, fetchConfig, {mode: 'no-cors'});
        if (response.ok) {
            setName('');
        }
    }

    useEffect(() => {
        }, [] );

        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a Manufacturer</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="name" required type="text" name="name" value={name} id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <button className="btn btn-primary">Create a manufacturer</button>
                </form>
                </div>
                </div>
            </div>
                );
                }

export default ManufacturerForm;

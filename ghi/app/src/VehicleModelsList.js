import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
function VehicleModelsList() {
    const [vehichleList, setVehicleList] = useState([]);
    useEffect(() => {
        loadVehichles();
    }, []);

    async function loadVehichles() {
        const response = await fetch('http://localhost:8100/api/models/');
        if(response.ok) {
            const data = await response.json();
            setVehicleList(data.models);
        }
        else {
            console.error(response);
        }
    }
return (
    <div>
        <br />
        <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
            <Link to='/inventory/models/new' className="btn btn-primary btn-lg px-4 gp-3">Create a Vehicle</Link>
        </div>
        <br />
        <h2>Vehicle Models</h2>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
        </tr>
        </thead>
        <tbody>
            {vehichleList.map(vehichleModel =>
                <tr key={ vehichleModel.id }>
                    <td>{ vehichleModel.name }</td>
                    <td>{ vehichleModel.manufacturer.name }</td>
                    <td><img src={ vehichleModel.picture_url }alt={ vehichleModel.name } width="200" height="200"/></td>
                </tr>
            ) }
        </tbody>
    </table>
    </div>
    );
}
export default VehicleModelsList;
